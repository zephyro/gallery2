<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="section__wrap">

                        <div class="section__heading mb_60">
                            <div class="section__heading_title">Личный кабинет</div>
                            <span class="section__heading_text">Имя Пользователя</span>
                        </div>

                        <form class="form">

                            <div class="account">
                                <div class="account__nav">
                                    <ul>
                                        <li><a href="#">Настройки</a></li>
                                        <li class="active"><a href="#">Профиль</a></li>
                                        <li><a href="#">Корзина</a></li>
                                        <li><a href="#">Избранные работы</a></li>
                                        <li><a href="#">Избранные авторы</a></li>
                                    </ul>
                                </div>
                                <div class="account__content">

                                    <div class="account_settings">

                                        <div class="form_group">
                                            <label class="form_label">Фамилия</label>
                                            <div class="form_wrap form_change">
                                                <input class="form_control" name="" type="text" placeholder="Попов">
                                            </div>
                                        </div>

                                        <div class="form_group">
                                            <label class="form_label">имя</label>
                                            <input class="form_control" name="" type="text" placeholder="Анатолий">
                                        </div>

                                        <div class="form_group">
                                            <label class="form_label">Отчество</label>
                                            <input class="form_control" name="" type="text" placeholder="Илларионович">
                                        </div>

                                        <div class="form_group">
                                            <label class="form_label">телефон</label>
                                            <input class="form_control form_phone" name="phone" type="text" placeholder="+7 (     )        -     -    ">
                                        </div>

                                        <div class="form_group">
                                            <label class="form_label">Адрес</label>
                                            <input class="form_control" name="" type="text" placeholder="г. Москва, Бумажный проезд, 14/1">
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="account_submit">
                                <button type="submit" class="btn btn_send">Сохранить</button>
                            </div>

                        </form>

                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

