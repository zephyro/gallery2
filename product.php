<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="section__wrap">

                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Collection</a></li>
                            <li>Syurina</li>
                        </ul>

                        <div class="product">

                            <div class="product__info">
                                <h1>Clos Luce. Leonardo</h1>
                                <div class="product__author">Margarita Syurina</div>
                                <div class="product__size">60x90</div>
                                <div class="product__type">Canvas, oil</div>
                                <div class="product__purchase">
                                    <div class="product_price">1500 - 2400 USD</div>
                                    <div class="product_button">
                                        <button class="btn" type="submit">SEND A BID</button>
                                    </div>
                                </div>
                                <h3>Description</h3>
                                <div class="product__description">
                                    W.S. Maugham, “Macintosh: A Story of the South Seas,” The Cosmopolitan, November 1920, pp. 14-15, illustrated.<br/>
                                    J.R. Schoonover, L.S. Smith, L. Dean, Frank E. Schoonover: Catalogue Raisonne, vol. I, New Castle, Delaware, 2009, p. 323
                                </div>
                            </div>

                            <div class="product__gallery">
                                <div class="product__gallery_slider">
                                    <div class="gallery_slider swiper-container">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <a href="images/product_01.jpg" class="product_image" data-fancybox="product">
                                                    <img src="images/product_01.jpg" class="img-fluid" alt="">
                                                    <span><span><i></i></span></span>
                                                </a>
                                            </div>
                                            <div class="swiper-slide">
                                                <a href="images/product_01.jpg" class="product_image" data-fancybox="product">
                                                    <img src="images/product_02.jpg" class="img-fluid" alt="">
                                                    <span><span><i></i></span></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="product__gallery_thumbs">
                                    <li class="item0 active">
                                        <a href="#" data-target="0">
                                            <img src="images/product_01.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                    <li class="item1">
                                        <a href="#" data-target="1">
                                            <img src="images/product_02.jpg" class="img-fluid" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <div class="heading"><span>Artist</span></div>

                        <div class="author">
                            <div class="author_photo">
                                <img src="images/author_photo.jpg" class="img-fluid" alt="">
                            </div>
                            <div class="author_info">
                                <ul>
                                    <li>Born in 1969 in Bangkok, Thailand.</li>
                                    <li>1981 - the beginning of painting in the creative workshop of the People's Artist of Ukraine V. P. Tsvetkova.</li>
                                    <li>1987 - graduated from the Moscow Secondary Art School at the Institute. VI Surikov.</li>
                                    <li>1994 - graduated from the Moscow State Academic Art Institute. VI Surikov, the workshop of the People's Artist of the USSR Tair Salakhov.</li>
                                    <li>1999 - Member of the Creative Union of Artists of Russia.</li>
                                    <li>2001 - Member of the International Art Fund.</li>
                                    <li>2010 - Corresponding Member of the International Academy of Culture and Art.</li>
                                    <li>Cavalier of the Silver Order "Serving Art".</li>
                                    <li>2012 - Silver medal of the TASHR for contribution to the national culture.</li>
                                    <li>2012 - the certificate of honor of the Central Committee of trade unions of workers of culture of the Russian Federation.</li>

                                </ul>
                            </div>
                        </div>

                        <div class="heading"><span>Other paintings by artist</span></div>

                        <div class="other">
                            <div class="other_row">
                                <div class="other_item">
                                    <a href="#">
                                        <div class="other_image">
                                            <img src="images/other_01.jpg" class="img-fluid" alt="">
                                        </div>
                                        <span>Букет васильков</span>
                                    </a>
                                </div>
                                <div class="other_item">
                                    <a href="#">
                                        <div class="other_image">
                                            <img src="images/other_02.jpg" class="img-fluid" alt="">
                                        </div>
                                        <span>Натюрморт с гранатами</span>
                                    </a>
                                </div>
                                <div class="other_item">
                                    <a href="#">
                                        <div class="other_image">
                                            <img src="images/other_03.jpg" class="img-fluid" alt="">
                                        </div>
                                        <span>Зимнее плодородие</span>
                                    </a>
                                </div>
                                <div class="other_item">
                                    <a href="#">
                                        <div class="other_image">
                                            <img src="images/other_04.jpg" class="img-fluid" alt="">
                                        </div>
                                        <span>Таллин. У башни «Толстая Маргарита»</span>
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

