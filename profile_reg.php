<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="section__wrap">

                        <form class="form">

                            <div class="account">
                                <div class="account__nav">
                                    <h2>Регистрация</h2>
                                </div>
                                <div class="account__content">

                                    <div class="account_settings">

                                        <div class="form_group">
                                            <label class="form_label">имя пользователя</label>
                                            <input class="form_control" type="text" name="" placeholder="PokrasLampas">
                                        </div>

                                        <div class="form_group">
                                            <label class="form_label">email</label>
                                            <input class="form_control" type="text" name="" placeholder="pokraslampas@yandex.ru">
                                        </div>

                                        <div class="form_group">
                                            <label class="form_label">телефон</label>
                                            <input class="form_control" type="text" name="phone" placeholder="8 (910) 782-05-53">
                                        </div>

                                        <div class="form_group">
                                            <label class="form_label">придумайте пароль</label>
                                            <div class="form_wrap form_ok">
                                                <input class="form_control" name="" type="password" placeholder="··········">
                                            </div>
                                        </div>

                                        <div class="form_group">
                                            <label class="form_label">повторите пароль</label>
                                            <div class="form_wrap form_ok">
                                                <input class="form_control" name="" type="password" placeholder="··········">
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="account_submit">
                                <button type="submit" class="btn btn_send">Сохранить</button>
                            </div>

                        </form>

                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

