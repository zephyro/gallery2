<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="section__wrap">

                        <ul class="breadcrumb">
                            <li><a href="#">Главная</a></li>
                            <li><a href="#">Художник</a></li>
                            <li>Елена Прудникова</li>
                        </ul>


                        <div class="main_heading">
                            <h1>Елена Прудникова</h1>
                            <ul class="main_view">
                                <li><span>Показать работы:</span></li>
                                <li><a href="#" class="active" data-target=".goods_slider">Галереей</a></li>
                                <li><a href="#" data-target=".goods_tile">Плиткой</a></li>
                            </ul>
                        </div>

                        <div class="author_goods">

                            <div class="author_goods_block goods_slider active">
                                <div class="gallery img_cover">
                                    <div class="author_gallery swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="gallery_elem" style="background-image: url('images/painting_01.jpg');">
                                                    <div class="gallery_content">
                                                        <div class="gallery_name">Автопортрет</div>
                                                        <div class="gallery_year">2004</div>
                                                        <div class="gallery_params">Бумага, соус<br/>50 х 60</div>
                                                        <a href="#" class="btn">Купить</a>
                                                        <ul class="gallery_action">
                                                            <li>
                                                                <label class="liked">
                                                                    <input type="checkbox" value="liked">
                                                                    <span>
                                                                <i class="fa fa-heart"></i>
                                                                <i class="fa fa-heart-o"></i>
                                                            </span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <a href="images/painting_01.jpg" class="btn_modal"><i class="fa fa-search-plus"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-share-alt"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="swiper-slide">
                                                <div class="gallery_elem" style="background-image: url('images/painting_02.jpg');">
                                                    <div class="gallery_content">
                                                        <div class="gallery_name">Автопортрет</div>
                                                        <div class="gallery_year">2004</div>
                                                        <div class="gallery_params">Бумага, соус<br/>50 х 60</div>
                                                        <a href="#" class="btn">Купить</a>
                                                        <ul class="gallery_action">
                                                            <li>
                                                                <label class="liked">
                                                                    <input type="checkbox" value="liked">
                                                                    <span>
                                                                <i class="fa fa-heart"></i>
                                                                <i class="fa fa-heart-o"></i>
                                                            </span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <a href="images/painting_02.jpg" class="btn_modal"><i class="fa fa-search-plus"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-share-alt"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="swiper-slide">
                                                <div class="gallery_elem" style="background-image: url('images/painting_03.jpg');">
                                                    <div class="gallery_content">
                                                        <div class="gallery_name">Автопортрет</div>
                                                        <div class="gallery_year">2004</div>
                                                        <div class="gallery_params">Бумага, соус<br/>50 х 60</div>
                                                        <a href="#" class="btn">Купить</a>
                                                        <ul class="gallery_action">
                                                            <li>
                                                                <label class="liked">
                                                                    <input type="checkbox" value="liked">
                                                                    <span>
                                                                <i class="fa fa-heart"></i>
                                                                <i class="fa fa-heart-o"></i>
                                                            </span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <a href="images/painting_03.jpg" class="btn_modal"><i class="fa fa-search-plus"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-share-alt"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="swiper-slide">
                                                <div class="gallery_elem" style="background-image: url('images/painting_04.jpg');">
                                                    <div class="gallery_content">
                                                        <div class="gallery_name">Автопортрет</div>
                                                        <div class="gallery_year">2004</div>
                                                        <div class="gallery_params">Бумага, соус<br/>50 х 60</div>
                                                        <a href="#" class="btn">Купить</a>
                                                        <ul class="gallery_action">
                                                            <li>
                                                                <label class="liked">
                                                                    <input type="checkbox" value="liked">
                                                                    <span>
                                                                <i class="fa fa-heart-o"></i>
                                                            </span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <a href="images/painting_04.jpg" class="btn_modal"><i class="fa fa-search-plus"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-share-alt"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Add Arrows -->
                                        <div class="gallery-next swiper-button-next"></div>
                                        <div class="gallery-prev swiper-button-prev"></div>
                                    </div>

                                </div>
                            </div>
                            <div class="author_goods_block goods_tile">

                                <div class="goods_row">

                                    <div class="goods_elem">
                                        <div class="goods">
                                            <a href="#" class="goods_image">
                                                <img src="images/goods_01.jpg" alt="">
                                                <ul class="goods_links">
                                                    <li>
                                                        <label class="liked">
                                                            <input type="checkbox" value="liked">
                                                            <span>
                                                        <i class="fa fa-heart"></i>
                                                        <i class="fa fa-heart-o"></i>
                                                    </span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <a class="goods_share" href="#"><i class="fa fa-share-alt"></i></a>
                                                    </li>
                                                </ul>
                                            </a>
                                            <div class="goods_content">
                                                <div class="goods_author"><a href="#">Маргарита Сюрина</a></div>
                                                <div class="goods_name"><a href="#">Клу-Люс. Леонардо</a></div>
                                                <div class="goods_size">60х90</div>
                                                <div class="goods_purchase">
                                                    <div class="goods_price">100000 - 150000 руб.</div>
                                                    <div class="goods_button">
                                                        <a href="#" class="btn">Купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="goods_elem">
                                        <div class="goods">
                                            <a href="#" class="goods_image">
                                                <img src="images/goods_02.jpg" alt="">
                                                <ul class="goods_links">
                                                    <li>
                                                        <label class="liked">
                                                            <input type="checkbox" value="liked">
                                                            <span>
                                                        <i class="fa fa-heart"></i>
                                                        <i class="fa fa-heart-o"></i>
                                                    </span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <a class="goods_share" href="#"><i class="fa fa-share-alt"></i></a>
                                                    </li>
                                                </ul>
                                            </a>
                                            <div class="goods_content">
                                                <div class="goods_author"><a href="#">Елена Прудникова</a></div>
                                                <div class="goods_name"><a href="#">Музыка</a></div>
                                                <div class="goods_size">213х214</div>
                                                <div class="goods_purchase">
                                                    <div class="goods_price">150000 руб.</div>
                                                    <div class="goods_button">
                                                        <a href="#" class="btn">Купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="goods_elem">
                                        <div class="goods">
                                            <a href="#" class="goods_image">
                                                <img src="images/goods_03.jpg" alt="">
                                                <ul class="goods_links">
                                                    <li>
                                                        <label class="liked">
                                                            <input type="checkbox" value="liked">
                                                            <span>
                                                        <i class="fa fa-heart"></i>
                                                        <i class="fa fa-heart-o"></i>
                                                    </span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <a class="goods_share" href="#"><i class="fa fa-share-alt"></i></a>
                                                    </li>
                                                </ul>
                                            </a>
                                            <div class="goods_content">
                                                <div class="goods_author"><a href="#">Владимир Бокарев</a></div>
                                                <div class="goods_name"><a href="#">Серия «Серебряный век»</a></div>
                                                <div class="goods_size">90х75</div>
                                                <div class="goods_purchase">
                                                    <div class="goods_price">150000 руб.</div>
                                                    <div class="goods_button">
                                                        <a href="#" class="btn">Купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="goods_elem">
                                        <div class="goods">
                                            <a href="#" class="goods_image">
                                                <img src="images/goods_04.jpg" alt="">
                                                <ul class="goods_links">
                                                    <li>
                                                        <label class="liked">
                                                            <input type="checkbox" value="liked">
                                                            <span>
                                                        <i class="fa fa-heart"></i>
                                                        <i class="fa fa-heart-o"></i>
                                                    </span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <a class="goods_share" href="#"><i class="fa fa-share-alt"></i></a>
                                                    </li>
                                                </ul>
                                            </a>
                                            <div class="goods_content">
                                                <div class="goods_author"><a href="#">Владимир Бокарев</a></div>
                                                <div class="goods_name"><a href="#">Серия «Серебряный век»</a></div>
                                                <div class="goods_size">90х75</div>
                                                <div class="goods_purchase">
                                                    <div class="goods_price">150000 руб.</div>
                                                    <div class="goods_button">
                                                        <a href="#" class="btn">Купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="goods_elem">
                                        <div class="goods">
                                            <a href="#" class="goods_image">
                                                <img src="images/goods_05.jpg" alt="">
                                                <ul class="goods_links">
                                                    <li>
                                                        <label class="liked">
                                                            <input type="checkbox" value="liked">
                                                            <span>
                                                        <i class="fa fa-heart"></i>
                                                        <i class="fa fa-heart-o"></i>
                                                    </span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <a class="goods_share" href="#"><i class="fa fa-share-alt"></i></a>
                                                    </li>
                                                </ul>
                                            </a>
                                            <div class="goods_content">
                                                <div class="goods_author"><a href="#">Виктор Жеребило</a></div>
                                                <div class="goods_name"><a href="#">Ветерок с океана</a></div>
                                                <div class="goods_size">60х80</div>
                                                <div class="goods_purchase">
                                                    <div class="goods_price">150000 руб.</div>
                                                    <div class="goods_button">
                                                        <a href="#" class="btn">Купить</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>


                        </div>


                        <div class="heading"><span>О художнике</span></div>

                        <div class="about tabs">
                            <ul class="about__nav tabs_nav">
                                <li class="active"><a href="#" data-target=".tab1">Биография</a></li>
                                <li><a href="#" data-target=".tab2">Выставки</a></li>
                                <li><a href="#" data-target=".tab3">Награды</a></li>
                            </ul>
                            <div class="about__content">
                                <div class="tabs_item tab1 active">
                                    <div class="about__info">
                                        <div class="about__info_photo">
                                            <img src="images/author_photo2.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="about__info_text">
                                            <p>Родилась 27 июля 1978 года в Чите. В 1997году закончила Читинское Музыкальное училище, художественно-оформительское отделение. 2000-2007 гг. — обучение в Академии художеств, институте им. И.Е. Репина на факультете живописи ( мастерская С.Д. Кичко, В.В. Загонека). Диплом «отлично», похвала ГАК. Член Союза Художников с 2008 года.</p>
                                            <p>
                                                Золотая медаль Российской Академии Художеств 2007 г.<br/>
                                                Дипломант Всероссийской Молодежной выставки в Москве ( ЦДХ) 2010 г.<br/>
                                                Диплом лауреата молодежной выставки «Надежда 8» СХ СПб. 2010 г.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                                <div class="tabs_item tab2">
                                    <div class="about__info">
                                        <div class="about__info_photo">
                                            <img src="images/author_photo2.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="about__info_text">
                                            <h4>Выставик</h4>
                                            <p>
                                                Золотая медаль Российской Академии Художеств 2007 г.<br/>
                                                Дипломант Всероссийской Молодежной выставки в Москве ( ЦДХ) 2010 г.<br/>
                                                Диплом лауреата молодежной выставки «Надежда 8» СХ СПб. 2010 г.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                                <div class="tabs_item tab3">
                                    <div class="about__info">
                                        <div class="about__info_photo">
                                            <img src="images/author_photo2.jpg" class="img-fluid" alt="">
                                        </div>
                                        <div class="about__info_text">
                                            <h4>Награды</h4>
                                            <p>
                                                Золотая медаль Российской Академии Художеств 2007 г.<br/>
                                                Дипломант Всероссийской Молодежной выставки в Москве ( ЦДХ) 2010 г.<br/>
                                                Диплом лауреата молодежной выставки «Надежда 8» СХ СПб. 2010 г.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="heading"><span>Вам могут понравиться</span></div>
                        <div class="users">
                            <div class="users__elem">
                                <div class="users__name">
                                    <i>
                                        <img src="images/user_avatar_01.jpg" class="img-fluid" alt="">
                                    </i>
                                    <span>Маргарита Сюрина</span>
                                </div>
                                <div class="users__content">
                                    <a href="#" class="users__image image_01">
                                        <img src="images/pic01.jpg" class="img-fluid" alt="">
                                    </a>
                                    <a href="#" class="users__image">
                                        <img src="images/pic02.jpg" class="img-fluid" alt="">
                                    </a>
                                    <a href="#" class="users__image">
                                        <img src="images/pic03.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="users__bottom">
                                    <a href="#">все работы</a>
                                </div>
                            </div>
                            <div class="users__elem">
                                <div class="users__name">
                                    <i>
                                        <img src="images/user_avatar_02.jpg" class="img-fluid" alt="">
                                    </i>
                                    <span>Виктор Бокарев</span>
                                </div>
                                <div class="users__content">
                                    <a href="#" class="users__image">
                                        <img src="images/pic04.jpg" class="img-fluid" alt="">
                                    </a>
                                    <a href="#" class="users__image">
                                        <img src="images/pic05.jpg" class="img-fluid" alt="">
                                    </a>
                                    <a href="#" class="users__image">
                                        <img src="images/pic06.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="users__bottom">
                                    <a href="#">все работы</a>
                                </div>
                            </div>
                            <div class="users__elem">
                                <div class="users__name">
                                    <i>
                                        <img src="images/user_avatar_03.jpg" class="img-fluid" alt="">
                                    </i>
                                    <span>Елена Прудникова</span>
                                </div>
                                <div class="users__content">
                                    <a href="#" class="users__image">
                                        <img src="images/pic07.jpg" class="img-fluid" alt="">
                                    </a>
                                    <a href="#" class="users__image">
                                        <img src="images/pic08.jpg" class="img-fluid" alt="">
                                    </a>
                                    <a href="#" class="users__image">
                                        <img src="images/pic09.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="users__bottom">
                                    <a href="#">все работы</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

