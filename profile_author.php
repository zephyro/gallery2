<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="section__wrap">

                        <div class="section__heading mb_60">
                            <div class="section__heading_title">Личный кабинет</div>
                            <span class="section__heading_text">Имя Художника</span>
                        </div>

                        <form class="form">

                            <div class="account">
                                <div class="account__nav">
                                    <ul>
                                        <li><a href="#">Настройки</a></li>
                                        <li class="active"><a href="#">Профиль</a></li>
                                        <li><a href="#">Мои работы</a></li>
                                        <li><a href="#">Корзина</a></li>
                                        <li><a href="#">Избранные работы</a></li>
                                        <li><a href="#">Избранные авторы</a></li>
                                    </ul>
                                </div>
                                <div class="account__content">

                                    <div class="profile">

                                        <div class="profile__row">

                                            <div class="profile__photo">
                                                <label class="profile_image">
                                                    <input type="file" name="photo">
                                                    <img src="images/account_photo.jpg" class="img-fluid" alt="">
                                                    <span><i></i></span>
                                                </label>
                                            </div>

                                            <div class="profile__info">
                                                <div class="profile_heading">
                                                    <span>основная информация</span>
                                                    <a href="#" class="profile_change"><i></i></a>
                                                </div>
                                                <div class="form_group">
                                                    <label class="form_label">фамилия</label>
                                                    <input class="form_control" type="text" name="" placeholder="Попов">
                                                </div>

                                                <div class="form_group">
                                                    <label class="form_label">имя</label>
                                                    <input class="form_control" type="text" name="" placeholder="Анатолий">
                                                </div>

                                                <div class="form_group">
                                                    <label class="form_label">отчество</label>
                                                    <input class="form_control" type="text" name="" placeholder="Илларионович">
                                                </div>

                                                <div class="form_group">
                                                    <label class="form_label">дата рождения</label>
                                                    <input class="form_control" type="text" name="" placeholder="08.12.1984">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="profile_group">

                                            <div class="profile_heading">
                                                <strong>биография</strong>
                                                <a href="#" class="profile_change"><i></i></a>
                                            </div>
                                            <div class="profile_data">
                                                <div class="profile_data_text">
                                                    Анатолий Попов родился в 1940 году в Смоленске. После обучения в Ярославском художественном училище, где преподавали талантливые педагоги, прошедшие школу Ленинградской Академии художеств, Попов вернулся в родной город. Анатолий Попов родился в 1940 году в Смоленске. После обучения в Ярославском художественном училище, где преподавали талантливые педагоги, прошедшие школу Ленинградской Академии художеств, Попов вернулся в родной город.
                                                </div>
                                                <a href="#" class="profile_data_toggle">
                                                    <span class="toggle_hide">развернуть</span>
                                                    <span class="toggle_show">свернуть</span>
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="profile_group">

                                            <div class="profile_heading">
                                                <strong>выставки</strong>
                                                <a href="#" class="profile_change"><i></i></a>
                                            </div>
                                            <div class="profile_data">
                                                <div class="profile_data_text">Золотая медаль Российской Академии Художеств 2007 г.<br/>
                                                    Дипломант Всероссийской Молодежной выставки в Москве ( ЦДХ) 2010 г.<br/>
                                                    Диплом лауреата молодежной выставки «Надежда 8» СХ СПб. 2010 г.<br/>
                                                    Золотая медаль Российской Академии Художеств 2007 г.<br/>
                                                    Дипломант Всероссийской Молодежной выставки в Москве ( ЦДХ) 2010 г.<br/>
                                                    Диплом лауреата молодежной выставки «Надежда 8» СХ СПб. 2010 г.
                                                </div>
                                                <a href="#" class="profile_data_toggle">
                                                    <span class="toggle_hide">развернуть</span>
                                                    <span class="toggle_show">свернуть</span>
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="profile_group">

                                            <div class="profile_heading">
                                                <strong>награды</strong>
                                                <a href="#" class="profile_change"><i></i></a>
                                            </div>
                                            <div class="profile_data">
                                                <div class="profile_data_text">
                                                    Золотая медаль Российской Академии Художеств 2007 г.<br/>
                                                    Дипломант Всероссийской Молодежной выставки в Москве ( ЦДХ) 2010 г.<br/>
                                                    Диплом лауреата молодежной выставки «Надежда 8» СХ СПб. 2010 г.<br/>
                                                    Золотая медаль Российской Академии Художеств 2007 г.<br/>
                                                    Дипломант Всероссийской Молодежной выставки в Москве ( ЦДХ) 2010 г.<br/>
                                                    Диплом лауреата молодежной выставки «Надежда 8» СХ СПб. 2010 г.
                                                </div>
                                                <a href="#" class="profile_data_toggle">
                                                    <span class="toggle_hide">развернуть</span>
                                                    <span class="toggle_show">свернуть</span>
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="account_submit">
                                <button type="submit" class="btn btn_send">Сохранить</button>
                            </div>

                        </form>

                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

