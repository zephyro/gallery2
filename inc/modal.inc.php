<!-- Registration -->
<div class="hide">
    <div class="modal" id="reg">
        <div class="modal_title">Регистрация</div>
        <form class="form">
            <div class="form_group">
                <div class="radio_group">
                    <label class="form_radio form_radio_dark">
                        <input type="radio" name="user_type" value="" checked>
                        <span>ценитель</span>
                    </label>
                    <label class="form_radio form_radio_dark">
                        <input type="radio" name="user_type" value="">
                        <span>художник</span>
                    </label>
                </div>
            </div>
            <div class="form_group">
                <label class="form_label">имя</label>
                <input class="form_control form_control_white form_control_md" type="text" name="" placeholder="PokrasLampas">
            </div>
            <div class="form_group">
                <label class="form_label">email</label>
                <input class="form_control form_control_white form_control_md" type="text" name="" placeholder="pokraslampas@mail.ru">
            </div>
            <div class="form_group">
                <label class="form_label">телефон</label>
                <input class="form_control form_control_white form_control_md" type="text" name="" placeholder="8 910 782-05-53">
            </div>
            <div class="form_group">
                <label class="form_label">придумайте пароль</label>
                <input class="form_control form_control_white form_control_password form_control_md" type="password" name="pass1" placeholder="··········">
            </div>
            <div class="form_group">
                <label class="form_label">повторите пароль</label>
                <input class="form_control form_control_white form_control_password form_control_md" type="password" name="pass2" placeholder="··········">
            </div>
            <div class="btn_group">
                <button type="submit" class="btn btn_white btn_md btn_send">Зарегистрироваться</button>
            </div>
        </form>
    </div>
</div>
<!-- -->