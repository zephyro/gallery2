<footer class="footer">
    <div class="container">

        <div class="footer__top">

            <nav class="footer__primary">
                <ul>
                    <li><a href="#">О проекте</a></li>
                    <li><a href="#">Коллекция</a></li>
                    <li><a href="#">Художники</a></li>
                    <li><a href="#">Мероприятия</a></li>
                </ul>
                <ul>
                    <li><a href="#">Купить картину</a></li>
                    <li><a href="#">Продать картину</a></li>
                    <li><a href="#">Заказать копию</a></li>
                    <li><a href="#">Помощь</a></li>
                </ul>
            </nav>

            <nav class="footer__second">
                <div class="footer__second_elem">
                    <div class="footer__second_title"><a href="#">Картины для интерьера</a></div>
                    <ul>
                        <li><a href="#">для ресторана</a></li>
                        <li><a href="#">для офиса</a></li>
                        <li><a href="#">для кабинета</a></li>
                        <li><a href="#">для гостиной</a></li>
                    </ul>
                </div>
                <div class="footer__second_elem">
                    <div class="footer__second_title"><a href="#">Картины в подарок</a></div>
                    <ul>
                        <li><a href="#">для мужчины</a></li>
                        <li><a href="#">для женщины</a></li>
                        <li><a href="#">для руководителя</a></li>
                        <li><a href="#">для семейной пары</a></li>
                    </ul>
                </div>
                <div class="footer__second_elem">
                    <div class="footer__second_title"><a href="#">Жанры картин</a></div>
                    <ul>
                        <li><a href="#">натюрморт</a></li>
                        <li><a href="#">пейзаж</a></li>
                        <li><a href="#">портрет</a></li>
                        <li><a href="#">батализм</a></li>
                    </ul>
                </div>
            </nav>

            <ul class="footer__social">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-vk"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            </ul>

        </div>

        <div class="footer__bottom">
            <a class="footer__logo" href="/">
                <img src="img/logo.png" class="img-fluid" alt="">
            </a>
            <div class="footer__subscribe">
                <div class="footer__subscribe_label">Подпишитесь на нашу рассылку и получайте свежие новости о проекте:</div>
                <div class="footer__subscribe_input">
                    <div class="footer__subscribe_form">
                        <form class="form">
                            <input type="text" name="subscribe" placeholder="email">
                            <button type="submit"><i class="fa fa-angle-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="footer__privacy">
                <a href="#">Политика обработки персональных данных</a>
            </div>
        </div>

    </div>
</footer>