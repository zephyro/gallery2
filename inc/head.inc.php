<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i&amp;subset=cyrillic" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
<link rel="stylesheet" href="js/vendor/swiper/css/swiper.min.css">
<link rel="stylesheet" href="js/vendor/ion.rangeSlider/css/ion.rangeSlider.css">
<link rel="stylesheet" href="js/vendor/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css">

<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/main.css">