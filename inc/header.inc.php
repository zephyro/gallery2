<header class="header">
    <div class="container">

        <div class="header__top">
            <ul class="header__link">
                <li>
                    <a href="#"><span>Язык</span> <i class="fa fa-caret-down"></i></a>
                    <ul class="dropdown dropdown_lng">
                        <li><a href="#" class="active"><span>Русский</span></a></li>
                        <li><a href="#"><span>English</span></a></li>
                        <li><a href="#"><span>Chinese</span></a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span>Username</span> <i class="fa fa-caret-down"></i></a>
                    <ul class="dropdown dropdown_settings">
                        <li><a href="#"><span>Settings</span></a></li>
                        <li><a href="#"><span>My cart <b>2</b></span></a></li>
                        <li><a href="#"><span>Favorite painting</span></a></li>
                        <li><a href="#"><span>Favorite artists</span></a></li>
                        <li><a href="#"><span>Log out</span></a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="header__bottom">
            <a class="header__logo" href="/">
                <img src="img/logo.png" class="img-fluid" alt="">
            </a>
            <div class="header__content">
                <ul class="header__nav">
                    <li><a href="#"><span>главная</span></a></li>
                    <li><a href="#"><span>коллекция</span></a></li>
                    <li><a href="#"><span>художники</span></a></li>
                    <li><a href="#"><span>выставки</span></a></li>
                    <li><a href="#"><span>контакты</span></a></li>
                </ul>
                <div class="header__search">
                    <form class="form">
                        <input type="text" class="header__search_input" name="" placeholder="Поиск по сайту">
                        <button type="submit" class="btn_search"></button>
                    </form>
                </div>
                <div class="header__content_close nav_toggle"></div>
            </div>
            <div class="header__layout nav_toggle"></div>
            <a href="#" class="header__toggle nav_toggle">
                <span></span>
                <span></span>
                <span></span>
            </a>
        </div>

    </div>
</header>