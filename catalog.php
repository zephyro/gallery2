<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">
                    <h1>Работы в коллекции<br/> Русской галереи</h1>

                    <div class="cat">

                        <div class="cat__filter">


                            <div class="filter">

                                <div class="filter_scroll">

                                    <div class="filter__box">
                                        <div class="filter__title">Параметры картины</div>

                                        <div class="filter__slider">
                                            <div class="filter__slider_legend">
                                                <strong>Цена, руб.</strong>
                                                <i>1500 - 500000</i>
                                            </div>
                                            <input type="text" class="form_slider" name="filter_price" value="" data-type="double" data-min="1000" data-max="100000" data-step="500" data-from="1500" data-to="50000" />
                                        </div>

                                        <div class="filter__slider">
                                            <div class="filter__slider_legend">
                                                <strong>Ширина, см</strong>
                                                <i>30 - 270</i>
                                            </div>
                                            <input type="text" class="form_slider" name="filter_width" value="" data-type="double" data-min="10" data-max="1000" data-step="5" data-from="30" data-to="270" />
                                        </div>

                                        <div class="filter__slider">
                                            <div class="filter__slider_legend">
                                                <strong>Высота, см</strong>
                                                <i>20 - 1000</i>
                                            </div>
                                            <input type="text" class="form_slider" name="filter_height" value="" data-type="double" data-min="10" data-max="1000" data-step="5" data-from="20" data-to="300" />
                                        </div>

                                    </div>

                                    <div class="filter__box">
                                        <div class="filter__title">Ориентация полотна</div>
                                        <div class="screen">
                                            <label class="screen_item">
                                                <input type="checkbox" class="screen" value="1">
                                                <span class="screen_album"></span>
                                            </label>
                                            <label class="screen_item">
                                                <input type="checkbox" class="screen" value="1">
                                                <span class="screen_portrait"></span>
                                            </label>
                                            <label class="screen_item">
                                                <input type="checkbox" class="screen" value="1">
                                                <span class="screen_square"></span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="filter__box open">
                                        <div class="filter__title">Цвета</div>
                                        <ul class="filter_list filter_colors">
                                            <li>
                                                <label>
                                                    <input type="checkbox" class="color" value="1">
                                                    <span>
                                                            <img src="img/colors/color_01.jpg" class="img-fluid" alt="">
                                                        </span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" class="color" value="1">
                                                    <span>
                                                            <img src="img/colors/color_02.jpg" class="img-fluid" alt="">
                                                        </span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" class="color" value="1">
                                                    <span>
                                                            <img src="img/colors/color_03.jpg" class="img-fluid" alt="">
                                                        </span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" class="color" value="1">
                                                    <span>
                                                            <img src="img/colors/color_04.jpg" class="img-fluid" alt="">
                                                        </span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" class="color" value="1">
                                                    <span>
                                                            <img src="img/colors/color_05.jpg" class="img-fluid" alt="">
                                                        </span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" class="color" value="1">
                                                    <span>
                                                            <img src="img/colors/color_06.jpg" class="img-fluid" alt="">
                                                        </span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" class="color" value="1">
                                                    <span>
                                                            <img src="img/colors/color_07.jpg" class="img-fluid" alt="">
                                                        </span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" class="color" value="1">
                                                    <span>
                                                            <img src="img/colors/color_08.jpg" class="img-fluid" alt="">
                                                        </span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" class="color" value="1">
                                                    <span>
                                                            <img src="img/colors/color_09.jpg" class="img-fluid" alt="">
                                                        </span>
                                                </label>
                                            </li>
                                        </ul>
                                        <a href="#" class="filter__toggle">
                                            <span class="toggle_hide">скрыть варианты</span>
                                            <span class="toggle_show">еще варианты</span>
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                    </div>

                                    <div class="filter__box">
                                        <div class="filter__title">Для интерьера</div>
                                        <ul class="filter_list filter_params">
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="filter_interior" value="" checked>
                                                    <span>Гостиная</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="filter_interior" value="" checked>
                                                    <span>Спальня</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="filter_interior" value="">
                                                    <span>Офис</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="filter_interior" value="">
                                                    <span>Кафе и ресторан</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="filter_interior" value="">
                                                    <span>Кухня</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="filter_interior" value="">
                                                    <span>Отель</span>
                                                </label>
                                            </li>
                                        </ul>
                                        <a href="#" class="filter__toggle">
                                            <span class="toggle_hide">скрыть варианты</span>
                                            <span class="toggle_show">еще варианты</span>
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                    </div>

                                    <div class="filter__box">
                                        <div class="filter__title">Кому подарить</div>
                                        <ul class="filter_list filter_params">
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="whom" value="" checked>
                                                    <span>Супругу</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="whom" value="" checked>
                                                    <span>Родителям</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="whom" value="">
                                                    <span>Начальнику</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="whom" value="">
                                                    <span>Друзьям</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="whom" value="">
                                                    <span>Коллегам</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="text" name="whom" value="">
                                                    <span>Партнерам</span>
                                                </label>
                                            </li>
                                        </ul>
                                        <a href="#" class="filter__toggle">
                                            <span class="toggle_hide">скрыть варианты</span>
                                            <span class="toggle_show">еще варианты</span>
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                    </div>

                                    <div class="filter__box">
                                        <div class="filter__title">Материал основы</div>
                                        <ul class="filter_list filter_params">
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="material" value="">
                                                    <span>Дерево</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="material" value="">
                                                    <span>Холст</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="material" value="">
                                                    <span>Металл</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="material" value="">
                                                    <span>Металл</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="material" value="">
                                                    <span>Пластик</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="material" value="">
                                                    <span>Камень</span>
                                                </label>
                                            </li>
                                        </ul>
                                        <a href="#" class="filter__toggle">
                                            <span class="toggle_hide">скрыть варианты</span>
                                            <span class="toggle_show">еще варианты</span>
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                    </div>

                                    <div class="filter__box">
                                        <div class="filter__title">Изобразительный материал</div>
                                        <ul class="filter_list filter_params">
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="paint" value="">
                                                    <span>Акварель</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="paint" value="">
                                                    <span>Масло</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="paint" value="">
                                                    <span>Акрил</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="paint" value="">
                                                    <span>Тушь</span>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="checkbox" name="paint" value="">
                                                    <span>Гуашь</span>
                                                </label>
                                            </li>
                                        </ul>
                                        <a href="#" class="filter__toggle">
                                            <span class="toggle_hide">скрыть варианты</span>
                                            <span class="toggle_show">еще варианты</span>
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cat__filter_layout filter_btn_toggle"></div>

                        <div class="cat__main">

                            <a href="#" class="filter_button  filter_btn_toggle">
                                <i class="fa fa-filter"></i>
                            </a>

                            <div class="cat__top">

                                <div class="cat__params">

                                    <div class="cat__params_item">
                                        <div class="select_box">
                                            <div class="select_box_value">
                                                <span>автор</span>
                                            </div>
                                            <div class="select_box_dropdown">
                                                <ul>
                                                    <li>
                                                        <label>
                                                            <input type="checkbox" value="" name="" checked>
                                                            <span>Андрей Попов</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="checkbox" value="" name="">
                                                            <span>Полина Смолова</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="checkbox" value="" name="">
                                                            <span>Дмитрий Иванов</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="cat__params_item">
                                        <div class="select_box">
                                            <div class="select_box_value">
                                                <span>Жанр</span>
                                            </div>
                                            <div class="select_box_dropdown">
                                                <ul>
                                                    <li>
                                                        <label>
                                                            <input type="checkbox" value="" name="">
                                                            <span>Портрет</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="checkbox" value="" name="">
                                                            <span>Пейзаж</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="checkbox" value="" name="">
                                                            <span>Натюрморт</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="cat__params_item">
                                        <div class="select_box">
                                            <div class="select_box_value">
                                                <span>стиль</span>
                                            </div>
                                            <div class="select_box_dropdown">
                                                <ul>
                                                    <li>
                                                        <label>
                                                            <input type="checkbox" value="" name="">
                                                            <span>Классика</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="checkbox" value="" name="">
                                                            <span>Модерн</span>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="checkbox" value="" name="">
                                                            <span>Абстракционизм</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="cat__bar">
                                    <ul class="cat__view">
                                        <li><span class="cat__view_elem">Цена<i></i></span></li>
                                        <li><span class="cat__view_elem invert">Год<i></i></span></li>
                                        <li>
                                            <div class="view_box">
                                                <label class="view_box_label">Показывать:</label>
                                                <div class="view_box_select">
                                                    <select class="form_select">
                                                        <option value="20">20</option>
                                                        <option value="50">40</option>
                                                        <option value="10">80</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="bar_search">
                                        <form class="form">
                                            <input type="text" class="bar_search_input" name="" placeholder="Поиск по ключевым словам">
                                            <button type="submit"><i class="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                </div>

                            </div>

                            <div class="goods_row">

                                <div class="goods_elem">
                                    <div class="goods">
                                        <a href="#" class="goods_image">
                                            <img src="images/goods_01.jpg" alt="">
                                            <ul class="goods_links">
                                                <li>
                                                    <label class="liked">
                                                        <input type="checkbox" value="liked">
                                                        <span>
                                                        <i class="fa fa-heart"></i>
                                                        <i class="fa fa-heart-o"></i>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <a class="goods_share" href="#"><i class="fa fa-share-alt"></i></a>
                                                </li>
                                            </ul>
                                        </a>
                                        <div class="goods_content">
                                            <div class="goods_author"><a href="#">Маргарита Сюрина</a></div>
                                            <div class="goods_name"><a href="#">Клу-Люс. Леонардо</a></div>
                                            <div class="goods_size">60х90</div>
                                            <div class="goods_purchase">
                                                <div class="goods_price">100000 - 150000 руб.</div>
                                                <div class="goods_button">
                                                    <a href="#" class="btn">Купить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="goods_elem">
                                    <div class="goods">
                                        <a href="#" class="goods_image">
                                            <img src="images/goods_02.jpg" alt="">
                                            <ul class="goods_links">
                                                <li>
                                                    <label class="liked">
                                                        <input type="checkbox" value="liked">
                                                        <span>
                                                        <i class="fa fa-heart"></i>
                                                        <i class="fa fa-heart-o"></i>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <a class="goods_share" href="#"><i class="fa fa-share-alt"></i></a>
                                                </li>
                                            </ul>
                                        </a>
                                        <div class="goods_content">
                                            <div class="goods_author"><a href="#">Елена Прудникова</a></div>
                                            <div class="goods_name"><a href="#">Музыка</a></div>
                                            <div class="goods_size">213х214</div>
                                            <div class="goods_purchase">
                                                <div class="goods_price">150000 руб.</div>
                                                <div class="goods_button">
                                                    <a href="#" class="btn">Купить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="goods_elem">
                                    <div class="goods">
                                        <a href="#" class="goods_image">
                                            <img src="images/goods_03.jpg" alt="">
                                            <ul class="goods_links">
                                                <li>
                                                    <label class="liked">
                                                        <input type="checkbox" value="liked">
                                                        <span>
                                                        <i class="fa fa-heart"></i>
                                                        <i class="fa fa-heart-o"></i>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <a class="goods_share" href="#"><i class="fa fa-share-alt"></i></a>
                                                </li>
                                            </ul>
                                        </a>
                                        <div class="goods_content">
                                            <div class="goods_author"><a href="#">Владимир Бокарев</a></div>
                                            <div class="goods_name"><a href="#">Серия «Серебряный век»</a></div>
                                            <div class="goods_size">90х75</div>
                                            <div class="goods_purchase">
                                                <div class="goods_price">150000 руб.</div>
                                                <div class="goods_button">
                                                    <a href="#" class="btn">Купить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="goods_elem">
                                    <div class="goods">
                                        <a href="#" class="goods_image">
                                            <img src="images/goods_04.jpg" alt="">
                                            <ul class="goods_links">
                                                <li>
                                                    <label class="liked">
                                                        <input type="checkbox" value="liked">
                                                        <span>
                                                        <i class="fa fa-heart"></i>
                                                        <i class="fa fa-heart-o"></i>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <a class="goods_share" href="#"><i class="fa fa-share-alt"></i></a>
                                                </li>
                                            </ul>
                                        </a>
                                        <div class="goods_content">
                                            <div class="goods_author"><a href="#">Владимир Бокарев</a></div>
                                            <div class="goods_name"><a href="#">Серия «Серебряный век»</a></div>
                                            <div class="goods_size">90х75</div>
                                            <div class="goods_purchase">
                                                <div class="goods_price">150000 руб.</div>
                                                <div class="goods_button">
                                                    <a href="#" class="btn">Купить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="goods_elem">
                                    <div class="goods">
                                        <a href="#" class="goods_image">
                                            <img src="images/goods_05.jpg" alt="">
                                            <ul class="goods_links">
                                                <li>
                                                    <label class="liked">
                                                        <input type="checkbox" value="liked">
                                                        <span>
                                                        <i class="fa fa-heart"></i>
                                                        <i class="fa fa-heart-o"></i>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <a class="goods_share" href="#"><i class="fa fa-share-alt"></i></a>
                                                </li>
                                            </ul>
                                        </a>
                                        <div class="goods_content">
                                            <div class="goods_author"><a href="#">Виктор Жеребило</a></div>
                                            <div class="goods_name"><a href="#">Ветерок с океана</a></div>
                                            <div class="goods_size">60х80</div>
                                            <div class="goods_purchase">
                                                <div class="goods_price">150000 руб.</div>
                                                <div class="goods_button">
                                                    <a href="#" class="btn">Купить</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="pagination">
                                <ul>
                                    <li><a href="#">1</a></li>
                                    <li><span>2</span></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

