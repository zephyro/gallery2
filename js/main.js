// --- Top Nav

$(function() {
    var pull = $('.nav_toggle');
    var menu = $('.page');

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('nav_open');
    });
});


$(".btn_modal").fancybox({
    'padding' : 0
});

$(document).ready(function() {

    var jumbotron = new Swiper('.jumbotron-slider', {
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        }
    });

    jumbotron.on('slideChange', function () {
        var slide = '.elem' + jumbotron.activeIndex;
        console.log(slide);
        $('.jumbotron__bg').find('.jumbotron__bg_elem').removeClass('active');
        $('.jumbotron__bg').find(slide).addClass('active');
    });


});

// ----- Маска ----------
jQuery(function($){
    $("input[name='phone']").mask("+7(999) 999-9999");
});


var gallery = new Swiper('.gallery_slider',{
});

gallery.on('slideChange', function () {
    var num = gallery.activeIndex;
    var item = '.item' + num;
    $('.product__gallery_thumbs').find('li').removeClass('active');
    $('.product__gallery_thumbs').find(item).addClass('active');
});

$('.product__gallery_thumbs li a').on('click touchstart', function(event){
    event.preventDefault();

    var index = $(this).attr("data-target");
    $(this).closest('ul').find('li').removeClass('active');
    $(this).closest('li').addClass('active');
    gallery.slideTo(index);
});

var author = new Swiper('.author_gallery', {
    spaceBetween: 270,
    loop: true,
    loopFillGroupWithBlank: true,
    breakpoints: {
        768: {
            spaceBetween: 80
        }
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});


var $range = $(".form_slider");

$range.ionRangeSlider({
    hide_min_max: true,
    hide_from_to: true
});

(function() {

    $('.filter__toggle').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.filter__box').toggleClass('open');
    });

}());


$('.cat__view_elem').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).toggleClass('invert');
});

$('.filter_btn_toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $('.cat').toggleClass('open');
});

$('.profile_data_toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.profile_data').toggleClass('open');
});


$('.select_box_value').on('click touchstart', function(e) {
    e.preventDefault();

    if($(this).closest('.select_box').hasClass('open')){
        $(this).closest('.select_box').toggleClass('open');
    }
    else {
        $('.select_box').removeClass('open');
        $(this).closest('.select_box').toggleClass('open');
    }
});



// hide dropdown

$('body').click(function (event) {

    if ($(event.target).closest(".select_box").length === 0) {
        $(".select_box").removeClass('open');
    }

});


 {

}

(function() {

    $('.main_view li a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("data-target"));

        $(this).closest('.main_view').find('a').removeClass('active');
        $(this).addClass('active');

        $('body').find('.author_goods_block').removeClass('active');
        $('body').find(tab).addClass('active');
    });

}());