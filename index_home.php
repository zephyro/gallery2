<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <header class="header">
                <div class="container">

                    <div class="header__top">
                        <ul class="header__link">
                            <li>
                                <a href="#"><span>Язык</span> <i class="fa fa-caret-down"></i></a>
                                <ul class="dropdown dropdown_lng">
                                    <li><a href="#" class="active"><span>Русский</span></a></li>
                                    <li><a href="#"><span>English</span></a></li>
                                    <li><a href="#"><span>Chinese</span></a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#reg" class="btn_modal">Создать акааунт</a>
                            </li>
                            <li>
                                <a href="#" class="menu_enter">Войти</a>
                            </li>
                        </ul>
                    </div>

                    <div class="header__bottom">
                        <a class="header__logo" href="/">
                            <img src="img/logo.png" class="img-fluid" alt="">
                        </a>
                        <div class="header__content">
                            <ul class="header__nav">
                                <li><a href="#"><span>главная</span></a></li>
                                <li><a href="#"><span>коллекция</span></a></li>
                                <li><a href="#"><span>художники</span></a></li>
                                <li><a href="#"><span>выставки</span></a></li>
                                <li><a href="#"><span>контакты</span></a></li>
                            </ul>
                            <div class="header__search">
                                <form class="form">
                                    <input type="text" class="header__search_input" name="" placeholder="Поиск по сайту">
                                    <button type="submit" class="btn_search"></button>
                                </form>
                            </div>
                            <div class="header__content_close nav_toggle"></div>
                        </div>
                        <div class="header__layout nav_toggle"></div>
                        <a href="#" class="header__toggle nav_toggle">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>

                </div>
            </header>

            <section class="jumbotron">
                <div class="jumbotron__bg">
                    <div class="jumbotron__bg_elem elem0 active" style="background-image: url('img/jumbotron.jpg');"></div>
                    <div class="jumbotron__bg_elem elem1" style="background-image: url('img/jumbotron.jpg');"></div>
                    <div class="jumbotron__bg_elem elem2" style="background-image: url('img/jumbotron.jpg');"></div>
                </div>
                <div class="container">
                    <div class="jumbotron__content">
                        <!-- Swiper -->
                        <div class="jumbotron-slider swiper-container">
                            <div class="swiper-wrapper">

                                <div class="swiper-slide">
                                    <div class="jumbotron__slide">
                                        <div class="jumbotron__slide_tag">Шедевры нашей коллекции</div>
                                        <div class="jumbotron__slide_author">Маргарита<br/> Сюрина</div>
                                        <div class="jumbotron__slide_name">Клу-Люс. Леонардо</div>
                                        <div class="jumbotron__slide_year">1995</div>
                                        <div class="jumbotron__slide_price">10.000 - 15.000 руб.</div>
                                        <a href="#" class="btn">Купить</a>
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="jumbotron__slide">
                                        <div class="jumbotron__slide_tag">Шедевры нашей коллекции</div>
                                        <div class="jumbotron__slide_author">Маргарита<br/> Сюрина</div>
                                        <div class="jumbotron__slide_name">Клу-Люс. Леонардо</div>
                                        <div class="jumbotron__slide_year">1995</div>
                                        <div class="jumbotron__slide_price">10.000 - 15.000 руб.</div>
                                        <a href="#" class="btn">Купить</a>
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="jumbotron__slide">
                                        <div class="jumbotron__slide_tag">Шедевры нашей коллекции</div>
                                        <div class="jumbotron__slide_author">Маргарита<br/> Сюрина</div>
                                        <div class="jumbotron__slide_name">Клу-Люс. Леонардо</div>
                                        <div class="jumbotron__slide_year">1995</div>
                                        <div class="jumbotron__slide_price">10.000 - 15.000 руб.</div>
                                        <a href="#" class="btn">Купить</a>
                                    </div>
                                </div>

                            </div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next"><span></span></div>
                            <div class="swiper-button-prev"><span></span></div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section section_one">
                <div class="container">
                    <div class="section__wrap">
                        <div class="section__heading">
                            <div class="section__heading_title">Подборки</div>
                            <a class="section__heading_link" href="#">все подборки</a>
                        </div>
                        <div class="section__subtitle">картины, которые не просто украсят стены, а наполнят пространство эмоциями</div>

                        <div class="collection">
                            <div class="collection__item">
                                <a href="#">
                                    <img src="img/collection_mood.jpg" class="img-fluid" alt="">
                                    <span>Настроение</span>
                                </a>
                            </div>
                            <div class="collection__item">
                                <a href="#">
                                    <img src="img/collection_expression.jpg" class="img-fluid" alt="">
                                    <span>Интерьер</span>
                                </a>
                            </div>
                            <div class="collection__item">
                                <a href="#">
                                    <img src="img/collection_gift.jpg" class="img-fluid" alt="">
                                    <span>В подарк</span>
                                </a>
                            </div>
                            <div class="collection__item">
                                <a href="#">
                                    <img src="img/collection_subject.jpg" class="img-fluid" alt="">
                                    <span>Сюжеты</span>
                                </a>
                            </div>
                            <div class="collection__item">
                                <a href="#">
                                    <img src="img/collection_colours.jpg" class="img-fluid" alt="">
                                    <span>Цветовые сочетания</span>
                                </a>
                            </div>
                            <div class="collection__item">
                                <a href="#">
                                    <img src="img/collection_style.jpg" class="img-fluid" alt="">
                                    <span>Стиль</span>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="section section_reg">
                <div class="container">
                    <div class="section__wrap">
                        <div class="reg_row">
                            <div class="reg_text">
                                <div class="section__heading">
                                    <div class="section__heading_title">Регистрация</div>
                                </div>
                                <div class="section__subtitle">станьте членом клуба ценителей и знатоков современного искусства</div>
                            </div>
                            <div class="reg_form">
                                <form class="form">
                                    <div class="form_group">
                                        <div class="radio_group">
                                            <label class="form_radio">
                                                <input type="radio" name="user_type" value="" checked>
                                                <span>ценитель</span>
                                            </label>
                                            <label class="form_radio">
                                                <input type="radio" name="user_type" value="">
                                                <span>художник</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form_group">
                                        <label class="form_label">имя</label>
                                        <input class="form_control form_control_white" type="text" name="" placeholder="Ваше имя">
                                    </div>
                                    <div class="form_group">
                                        <label class="form_label">email</label>
                                        <input class="form_control form_control_white" type="text" name="" placeholder="Ваш Email">
                                    </div>
                                    <div class="form_group">
                                        <label class="form_label">телефон</label>
                                        <input class="form_control form_control_white" type="text" name="" placeholder="Ваш телефон">
                                    </div>
                                    <div class="btn_group">
                                        <button type="submit" class="btn btn_white btn_md btn_send">Зарегистрироваться</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section section_two">
                <div class="container">
                    <div class="section__wrap">
                        <div class="section__heading">
                            <div class="section__heading_title">Участники проекта</div>
                            <a class="section__heading_link" href="#">все художники</a>
                        </div>

                        <div class="users">
                            <div class="users__elem">
                                <div class="users__name">
                                    <i>
                                        <img src="images/user_avatar_01.jpg" class="img-fluid" alt="">
                                    </i>
                                    <a href="#">Маргарита Сюрина</a>
                                </div>
                                <div class="users__content">
                                    <a href="#" class="users__image image_01">
                                        <img src="images/pic01.jpg" class="img-fluid" alt="">
                                    </a>
                                    <a href="#" class="users__image">
                                        <img src="images/pic02.jpg" class="img-fluid" alt="">
                                    </a>
                                    <a href="#" class="users__image">
                                        <img src="images/pic03.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="users__bottom">
                                    <a href="#">все работы</a>
                                </div>
                            </div>
                            <div class="users__elem">
                                <div class="users__name">
                                    <i>
                                        <img src="images/user_avatar_02.jpg" class="img-fluid" alt="">
                                    </i>
                                    <a href="#">Виктор Бокарев</a>
                                </div>
                                <div class="users__content">
                                    <a href="#" class="users__image">
                                        <img src="images/pic04.jpg" class="img-fluid" alt="">
                                    </a>
                                    <a href="#" class="users__image">
                                        <img src="images/pic05.jpg" class="img-fluid" alt="">
                                    </a>
                                    <a href="#" class="users__image">
                                        <img src="images/pic06.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="users__bottom">
                                    <a href="#">все работы</a>
                                </div>
                            </div>
                            <div class="users__elem">
                                <div class="users__name">
                                    <i>
                                        <img src="images/user_avatar_03.jpg" class="img-fluid" alt="">
                                    </i>
                                    <a href="#">Елена Прудникова</a>
                                </div>
                                <div class="users__content">
                                    <a href="#" class="users__image">
                                        <img src="images/pic07.jpg" class="img-fluid" alt="">
                                    </a>
                                    <a href="#" class="users__image">
                                        <img src="images/pic08.jpg" class="img-fluid" alt="">
                                    </a>
                                    <a href="#" class="users__image">
                                        <img src="images/pic09.jpg" class="img-fluid" alt="">
                                    </a>
                                </div>
                                <div class="users__bottom">
                                    <a href="#">все работы</a>
                                </div>
                            </div>
                        </div>

                        <div class="services">
                            <div class="services__item item_01">
                                <a href="#">
                                    <h4>Как продать картину</h4>
                                    <span>для художников</span>
                                </a>
                            </div>
                            <div class="services__item item_02">
                                <a href="#">
                                    <h4>Как купить картину</h4>
                                    <span>для зрителей</span>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="section section_three">
                <div class="container">
                    <div class="section__wrap">

                        <div class="section__heading">
                            <div class="section__heading_title">Статьи</div>
                            <a class="section__heading_link" href="#">архив материалов</a>
                        </div>

                        <div class="articles">
                            <div class="articles__primary">
                               <a href="#">
                                   <img src="images/article_01.jpg" class="img-fluid" alt="">
                                   <span>Где купить современное искусство, если вы не миллионер</span>
                               </a> 
                            </div>
                            <div class="articles__list">
                                <a href="#" class="articles__item">
                                    <div class="articles__item_image">
                                        <img src="images/article_02.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="articles__item_text">
                                        <span>Вышел список самых дорогих работ ныне живущих художников</span>
                                    </div>
                                </a>
                                <a href="#" class="articles__item">
                                    <div class="articles__item_image">
                                        <img src="images/article_03.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="articles__item_text">
                                        <span>27 главных выставок, биеннале и фестивалей осени</span>
                                    </div>
                                </a>
                                <a href="#" class="articles__item">
                                    <div class="articles__item_image">
                                        <img src="images/article_04.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="articles__item_text">
                                        <span>27 главных выставок, биеннале и фестивалей осени</span>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="section section_four">
                <div class="container">
                    <div class="section__wrap">
                        <div class="section__heading">
                            <div class="section__heading_title">Мероприятия</div>
                        </div>
                        <div class="events">
                            <div class="events__item">
                                <a href="#">
                                    <img src="images/event_01.jpg" class="img-fluid" alt="">
                                    <span>Конкурс «Пас, Удар, Гол!!!»</span>
                                </a>
                            </div>
                            <div class="events__item">
                                <a href="#">
                                    <img src="images/event_02.jpg" class="img-fluid" alt="">
                                    <span>Как прошел конкурс «Залп «Авроры»</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

