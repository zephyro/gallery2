<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="section__wrap">

                        <div class="section__heading mb_60">
                            <div class="section__heading_title">Личный кабинет</div>
                            <span class="section__heading_text">Имя Пользователя</span>
                        </div>

                        <form class="form">

                            <div class="account">
                                <div class="account__nav">
                                    <ul>
                                        <li><a href="#">Настройки</a></li>
                                        <li class="active"><a href="#">Корзина</a></li>
                                        <li><a href="#">Избранные работы</a></li>
                                        <li><a href="#">Избранные авторы</a></li>
                                    </ul>
                                </div>
                                <div class="account__content">

                                    <div class="cart">

                                        <div class="cart_elem">
                                            <div class="cart_image">
                                                <a href="#">
                                                    <img src="images/basket_01.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="cart_info">
                                                <div class="cart_info_author"><a href="#">Маргарита Сюрина</a></div>
                                                <div class="cart_info_name"><a href="#">Клу-Люс. Леонардо</a></div>
                                                <div class="cart_info_size">60x90</div>
                                                <div class="cart_info_type">Холст, масло</div>
                                                <div class="cart_info_price">150000 - 240000 руб.</div>
                                                <span class="cart_remove"><i class="fa fa-trash"></i></span>
                                            </div>
                                        </div>

                                        <div class="cart_elem">
                                            <div class="cart_image">
                                                <a href="#">
                                                    <img src="images/basket_02.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="cart_info">
                                                <div class="cart_info_author"><a href="#">Владимир Бокарев</a></div>
                                                <div class="cart_info_name"><a href="#">Серия «Серебряный век»</a></div>
                                                <div class="cart_info_size">60x90</div>
                                                <div class="cart_info_type">Холст, масло</div>
                                                <div class="cart_info_price">150000 - 240000 руб.</div>
                                                <span class="cart_remove"><i class="fa fa-trash"></i></span>
                                            </div>
                                        </div>

                                        <div class="cart_elem">
                                            <div class="cart_image">
                                                <a href="#">
                                                    <img src="images/basket_03.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="cart_info">
                                                <div class="cart_info_author"><a href="#">Марк Ротко</a></div>
                                                <div class="cart_info_name"><a href="#">№10</a></div>
                                                <div class="cart_info_size">60x90</div>
                                                <div class="cart_info_type">Холст, масло</div>
                                                <div class="cart_info_price">150000 - 240000 руб.</div>
                                                <span class="cart_remove"><i class="fa fa-trash"></i></span>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="cart_submit">
                                <div class="cart_submit_title">Подтверждение заказа</div>
                                <div class="cart_submit_text">
                                    <p>После того, как Вы подтвердите заказ, данные о содержании заказа и ваши контактные данные будут отправлены нашим менеджерам, которые свяжутся с Вами для уточнения деталей заказа.</p>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn_send">Сохранить</button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

