<!doctype html>
<html class="no-js" lang="">

    <!-- Head -->
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <!-- -->

    <body>

        <div class="page">

            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <section class="main">
                <div class="container">
                    <div class="section__wrap">

                        <div class="section__heading mb_60">
                            <div class="section__heading_title">Личный кабинет</div>
                            <span class="section__heading_text">Имя Пользователя</span>
                        </div>

                        <form class="form">

                            <div class="account">
                                <div class="account__nav">
                                    <ul>
                                        <li class="active"><a href="#">Настройки</a></li>
                                        <li><a href="#">Профиль</a></li>
                                        <li><a href="#">Корзина</a></li>
                                        <li><a href="#">Избранные работы</a></li>
                                        <li><a href="#">Избранные авторы</a></li>
                                    </ul>
                                </div>
                                <div class="account__content">

                                    <div class="account_settings">

                                        <div class="form_group">
                                            <label class="form_label">email / логин</label>
                                            <input class="form_control" type="text" name="" placeholder="pokraslampas@yandex.ru">
                                        </div>

                                        <div class="form_group">
                                            <label class="form_label">пароль</label>
                                            <div class="form_wrap form_pass_view">
                                                <input class="form_control" name="" type="password" placeholder="··········">
                                                <span class="btn_pass_view"><i></i></span>
                                            </div>

                                        </div>

                                        <div class="form_group text-center">
                                            <a href="#" class="password_change">сменить пароль</a>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="account_submit">
                                <button type="submit" class="btn btn_send">Сохранить</button>
                            </div>

                        </form>

                    </div>
                </div>
            </section>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Modal -->
        <?php include('inc/modal.inc.php') ?>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

